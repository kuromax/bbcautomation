package com.frameworksample.pages.fragments;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Serhii Korol
 * Date: 27.01.2016
 */
public class AbstractFragment {

	public static final int DEFAULT_TIME_OUT_IN_SECONDS = 20;

	protected final WebDriver driver;
	protected final WebDriverWait wait;

	public AbstractFragment(WebDriver driver) {
		this.driver = driver;
		this.wait = new WebDriverWait(driver, DEFAULT_TIME_OUT_IN_SECONDS);
		PageFactory.initElements(driver, this);
	}

}
