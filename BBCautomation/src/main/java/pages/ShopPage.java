package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by mrybalkin on 09.01.2016.
 */
public class ShopPage extends BasePage{
    private static final String URL = "http://www.bbcshop.com/";

    @FindBy(id = "fullWidthDelMessage")
    private WebElement freeUkDeliveryText;

    public ShopPage(WebDriver driver){
        super(driver);
    }

    public String getShopPageUrl() {
        return URL;
    }

    public WebElement getFreeUkDeliveryText(){
        return freeUkDeliveryText;
    }

    public boolean isFreeUkDeliveryTextIsDisplayed(){
        return freeUkDeliveryText.isDisplayed();
    }
}
