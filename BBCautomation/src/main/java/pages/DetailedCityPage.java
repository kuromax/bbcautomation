package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by mrybalkin on 12.01.2016.
 */
public class DetailedCityPage extends BasePage {

    @FindBy(xpath = ".//*[@class='location-name']")
    private WebElement locationName;

    @FindBy(xpath = ".//*[@class='maps-tabs']")
    private WebElement mapsTabs;

    @FindBy(id = "average-conditions")
    private WebElement averageConditions;

    @FindBy(xpath = ".//*[@class='forecast-videos module  ']")
    private WebElement forecastVideosModule;

    public DetailedCityPage(WebDriver driver) {
        super(driver);
    }

    public String getLocationName(){
        return locationName.getText();
    }

    public boolean isMapsTabsBlockDisplayed(){
        return mapsTabs.isDisplayed();
    }

    public boolean isAverageConditionsBlockDisplayed(){
        return averageConditions.isDisplayed();
    }

    public boolean isForeCastVideosModuleDisplayed(){
        return forecastVideosModule.isDisplayed();
    }

}
