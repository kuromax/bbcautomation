package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by mrybalkin on 09.01.2016.
 */
public class NewsPage extends BasePage{
    private static final String URL = "http://www.bbc.com/news";

    private WebDriver driver;

    @FindBy(xpath = ".//*[@id='comp-pattern-library']//*[@class='group-title ']")
    private WebElement watchListenText;

    public NewsPage(WebDriver driver){
        super(driver);
    }

    public String getNewsPageUrl() {
        return URL;
    }

    public WebElement getWatchListenText(){
        return watchListenText;
    }

    public boolean isWatchListenTextDisplayed(){
        return watchListenText.isDisplayed();
    }
}
