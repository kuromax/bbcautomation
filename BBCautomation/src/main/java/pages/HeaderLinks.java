package pages;

/**
 * Created by mrybalkin on 21.01.2016.
 */
public enum HeaderLinks {
    NEWS_LINK(".//*[@id='orb-nav-links']//*[text()='News']"),
    SPORT_LINK(".//*[@id='orb-nav-links']//*[text()='Sport']"),
    WEATHER_LINK(".//*[@id='orb-nav-links']//a[text()='Weather']"),
    SHOP_LINK(".//*[@id='orb-nav-links']//a[text()='Shop']");

    private String name;

    HeaderLinks(String linkName) {
        this.name = linkName;
    }

    public String getLinkName(){
        return this.name;
    }
}
