package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * Created by mrybalkin on 21.12.2015.
 */
public class MainPage extends BasePage {
    public static final String URL = "http://www.bbc.com";

    @FindBy(xpath = ".//*[@id='orb-nav-links']//*[text()='News']")
    private WebElement newsLink;

    @FindBy(xpath = ".//*[@id='orb-nav-links']//*[text()='Sport']")
    private WebElement sportLink;

    @FindBy(xpath = ".//*[@id='orb-nav-links']//a[text()='Weather']")
    private WebElement weatherLink;

    @FindBy(xpath = ".//*[@id='orb-nav-links']//a[text()='Shop']")
    private WebElement shopLink;

    public MainPage(WebDriver driver){
        super(driver);
    }

    public MainPage open(){
        driver.get(URL);
        return this;
    }

    public NewsPage clickOnNewsLink(){
        newsLink.click();

        NewsPage newsPage = new NewsPage(driver);
        wait.until(ExpectedConditions.visibilityOf(newsPage.getWatchListenText()));

        return newsPage;
    }

    public SportPage clickOnSportLink(){
        sportLink.click();

        SportPage sportPage = new SportPage(driver);
        wait.until(ExpectedConditions.visibilityOf(sportPage.getVideoAndAudioText()));

        return sportPage;
    }

    public WeatherPage clickOnWeatherLink(){
        weatherLink.click();

        WeatherPage weatherPage = new WeatherPage(driver);
        wait.until(ExpectedConditions.visibilityOf(weatherPage.getSearchInputField()));

        return weatherPage;
    }

    public ShopPage clickOnShopLink() {
        shopLink.click();

        ShopPage shopPage = new ShopPage(driver);
        wait.until(ExpectedConditions.visibilityOf(shopPage.getFreeUkDeliveryText()));

        return shopPage;
    }

    public void clickOnLink(HeaderLinks links){
        driver.findElement(By.xpath(links.getLinkName())).click();
    }
}