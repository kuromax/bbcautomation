package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by mrybalkin on 09.01.2016.
 */
public class SportPage extends BasePage{
    private static final String URL = "http://www.bbc.com/sport/0/";

    @FindBy(xpath = "//*[@id='highlights-reports']//*[@class='carousel-header']")
    private WebElement videoAndAudioText;

    public SportPage(WebDriver driver){
        super(driver);
    }

    public String getSportPageUrl() {
        return URL;
    }

    public WebElement getVideoAndAudioText(){
        return videoAndAudioText;
    }

    public boolean isVideoAudioTextDisplayed(){
        return videoAndAudioText.isDisplayed();
    }
}
