package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

/**
 * Created by mrybalkin on 25.12.2015.
 */
public class WeatherPage extends BasePage {
    private static final String URL = "http://www.bbc/com/weather";

    @FindBy(id = "locator-form-search")
    private WebElement searchInputField;

    @FindBy(id = "locator-form-submit")
    private WebElement searchSubmitButton;

    @FindBy(xpath = ".//*[@class='columns']//*[text()='Forecast Video']")
    private WebElement weatherVideoText;

    @FindBy(xpath = ".//*[@id='find-a-forecast']/*[text()='Find a Forecast']")
    private WebElement findForecastText;

    @FindBy(xpath = ".//*[@id='summary']/*[@class='text']")
    private WebElement worldSummaryText;

    @FindBy(xpath = ".//*[text()='Features & Analysis']")
    private WebElement featuresAndAnalysisText;

    @FindBy(xpath = ".//*[@id='blq-content']//*[text()='In Pictures']")
    private WebElement inPicturesText;

    @FindBy(xpath = ".//*[@id='related-links']//*[text()='Related Links']")
    private WebElement relatedLinksText;

    @FindBy(xpath = ".//*[@id='related-links']//*[text()='About BBC Weather']")
    private WebElement aboutBbcWeatherText;

    @FindBy(xpath = ".//*[@class='locator-results']//*")
    private List<WebElement> linksInSearchText;


    public WeatherPage(WebDriver driver){
        super(driver);
    }

    public WeatherPage openBbcWeatherPage(){
        driver.get(URL);

        return this;
    }

    public String getWeatherLinkUrl(){
        return URL;
    }

    public WebElement getSearchInputField(){
        return searchInputField;
    }

    public WeatherPage search(String txt){
        searchInputField.clear();
        searchInputField.sendKeys(txt);
        searchSubmitButton.click();

        return this;
    }

    public boolean isLinkDisplayed(String linkText){
        return driver.findElements(By.xpath(".//*[@id='locator-search-default']//*[.='" + linkText + "']")).size() > 0;
    }

    public void clickOnLink(String linkTxt){
        driver.findElement(By.xpath(".//*[@id='locator-search-default']//*[.='" + linkTxt + "']")).click();
    }

    public boolean isWeatherVideoTextDisplayed(){
        return weatherVideoText.isDisplayed();
    }

    public boolean isFindForecastTextDisplayed(){
        return findForecastText.isDisplayed();
    }

    public boolean isWorldSummaryTextDisplayed(){
        return worldSummaryText.isDisplayed();
    }

    public boolean isFeaturesAndAnalysisTextDisplayed(){
        return featuresAndAnalysisText.isDisplayed();
    }

    public boolean isInPicturesTextDisplayed(){
        return inPicturesText.isDisplayed();
    }

    public boolean isRelatedLinksTextDisplayed(){
        return relatedLinksText.isDisplayed();
    }

    public boolean isAboutBbcWeatherText(){
        return aboutBbcWeatherText.isDisplayed();
    }

    public List<WebElement> getListOfSearchResults(){
        return linksInSearchText;
    }

    public DetailedCityPage clickCityLink(String city){
        List<WebElement> list = getListOfSearchResults();
        DetailedCityPage detailedCityPage = new DetailedCityPage(driver);
        if (list.size() > 0){
            for (int i = 0; i < list.size(); i ++) {
                if (list.get(i).getText().equals(city)){
                    list.get(i).click();
                }
            }
        }

        return detailedCityPage;
    }
}