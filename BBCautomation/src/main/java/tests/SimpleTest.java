package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by mrybalkin on 14.12.2015.
 */
public class SimpleTest {
    private static final String ID_ORB_NAV_LINKS_A_TEXT_NEWS = "";
    final String XPATH_NEWS_LINK = ".//*[@id='orb-nav-links']//*[text()='News']";
    //start browser FF
    private WebDriver driver;

    @BeforeTest
    public void beforeTest(){
        driver  = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    }

    @Test
    public void testBbcNews() throws InterruptedException {
        driver.get("http://www.bbc.com/");
        //find sport link
        List<WebElement> expectedElements = driver.findElements(By.xpath(XPATH_NEWS_LINK));

        String expectedUrl = "http://www.bbc.com/news";
        Thread.sleep(1000);
        expectedElements.get(0).click();

        String actualUrl = driver.getCurrentUrl();

        Assert.assertEquals(actualUrl, expectedUrl, "URLs are different");
    }

//
//    @Test
//    public void testBbcSport(){
//        //find sport link
//        WebElement sportsLink = driver.findElement(By.xpath(".//*[@id='orb-nav-links']//*[text()='Sport']"));
//
//        String expectedUrl = "http://www.bbc.com/sport/0/";
//
//        String actualUrl = driver.getCurrentUrl();
//
//        //use webelement
//        sportsLink.click();
//
//        if(expectedUrl.equals(actualUrl)){
//            System.out.println("YEESS!!");
//        }else{
//            System.out.println("NOOO!!!!");
//        }
//    }
//
//    public void testBbcWeather(){
//        //find sport link
//        WebElement sportsLink = driver.findElement(By.xpath(".//*[@id='orb-nav-links']//*[text()='Weather']"));
//
//        String expectedUrl = "http://www.bbc.com/weather/";
//
//        String actualUrl = driver.getCurrentUrl();
//
//        //use webelement
//        sportsLink.click();
//
//        if(expectedUrl.equals(actualUrl)){
//            System.out.println("YEESS!!");
//        }else{
//            System.out.println("NOOO!!!!");
//        }
//    }
//
//    public void testBbcShop(){
//        //find sport link
//        WebElement sportsLink = driver.findElement(By.xpath(".//*[@id='orb-nav-links']//*[text()='Shop']"));
//
//        String expectedUrl = "http://www.bbcshop.com/";
//
//        String actualUrl = driver.getCurrentUrl();
//
//        //use webelement
//        sportsLink.click();
//
//        if(expectedUrl.equals(actualUrl)){
//            System.out.println("YEESS!!");
//        }else{
//            System.out.println("NOOO!!!!");
//        }
//    }
//
//    public void testBbcEarth(){
//        //find sport link
//        WebElement sportsLink = driver.findElement(By.xpath(".//*[@id='orb-nav-links']//*[text()='Earth']"));
//
//        String expectedUrl = "http://www.bbc.com/earth/world";
//
//        String actualUrl = driver.getCurrentUrl();
//
//        //use webelement
//        sportsLink.click();
//
//        if(expectedUrl.equals(actualUrl)){
//            System.out.println("YEESS!!");
//        }else{
//            System.out.println("NOOO!!!!");
//        }
//    }

    public void bbcComNews() {
        driver.get("http://www.bbc.com/"); //navigate to url - bbc landing page
        WebElement newsLink = driver.findElement(By.xpath(ID_ORB_NAV_LINKS_A_TEXT_NEWS)); //find 'News' button
        newsLink.click(); //clicking on News button

        String expectedURL = "http://www.bbc.com/news"; //set expected result - url that we want to see when clicking on News button
        String actualURL = driver.getCurrentUrl(); // set actual result - the actual url that we get when click on News button
        Assert.assertEquals(actualURL, expectedURL, "URLs are different");
    }

    @Test
    public void idXpathTest(){
        driver.get("http://habrahabr.ru/");
        SoftAssert softAssert = new SoftAssert();
        List<WebElement> list = driver.findElements(By.xpath(".//*[@class='posts shortcuts_items']/*[contains(@id, 'post_')]"));
        list.get(0).findElement(By.xpath(".//*[@class='button habracut'"));
        softAssert.assertTrue(true);
        softAssert.assertAll();
    }

    @AfterTest
    public void afterTest(){
        driver.quit();
    }
}