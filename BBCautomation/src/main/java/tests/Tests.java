package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.MainPage;

import java.util.concurrent.TimeUnit;

/**
 * Created by mrybalkin on 21.12.2015.
 */
public class Tests {
    private WebDriver driver;

    @BeforeTest
    private void beforeTest(){
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @BeforeMethod
    private void beforeMethod() {

    }

    @Test
    public void test(){
        MainPage mainPage = new MainPage(driver);
        mainPage.clickOnNewsLink();
    }
}
