package tests;

import org.testng.annotations.*;
import pages.DetailedCityPage;
import pages.MainPage;
import pages.WeatherPage;

/**
 * Created by mrybalkin on 20.12.2015.
 */
public class WeatherTests extends AbstractTest {

    protected MainPage mainPage;

    @BeforeMethod
    protected void beforeMethod() {
        super.beforeMethod();
        mainPage = new MainPage(driver);
    }

    @DataProvider(name = "searchTest")
    public static Object[][] searchTest() {
        return new Object[][] {
                {"New York", "NEW YORK", "http://www.bbc.com/weather/5128581"},
                {"London", "London, Greater London", "http://www.bbc.com/weather/2643743"},
                {"Paris", "Paris Orly Airport, France", "http://www.bbc.com/weather/2988500"},
                {"Budapest", "Budapest, Hungary", "http://www.bbc.com/weather/3054643"}
        };
    }

    /**
     * Test verifies "Weather" page contains mandatory texts.
     *
     * Note: softAssert is used in test.
     *
     *  Steps:
     *  1. Navigate to bbc.com
     *  2. Click [Weather] button
     *  3. {Verify} Page contains 'Find a Forecast' text
     *  4. {Verify} Page contains 'World Summary' text
     *  5. {Verify} Page contains 'Features & Analysis' text
     *  6. {Verify} Page contains 'In Pictures' text
     *  7. {Verify} Page contains 'Related Links' text
     *  8. {Verify} Page contains 'About BBC Weather' text
     */
    @Test
    public void testTextOverview(){
        WeatherPage weatherPage = mainPage.open()
                .clickOnWeatherLink();

        softAssert.assertTrue(weatherPage.isFindForecastTextDisplayed(), "Find a Forecast text is not displayed");
        softAssert.assertTrue(weatherPage.isWorldSummaryTextDisplayed(), "World Summary text is not displayed");
        softAssert.assertTrue(weatherPage.isFeaturesAndAnalysisTextDisplayed(), "Feature and Analysis text is not displayed");
        softAssert.assertTrue(weatherPage.isInPicturesTextDisplayed(), "In pictures text is not displayed");
        softAssert.assertTrue(weatherPage.isRelatedLinksTextDisplayed(), "Related Links text is not displayed");
        softAssert.assertTrue(weatherPage.isAboutBbcWeatherText(), "About BBC weather text is not displayed");
        softAssert.assertAll();
    }

    /**
     * Test verifies:
     * 1. is possible to search certain city on "Weather" page
     * 2. the User redirects to the correct page with mandatory elements
     *
     * Note: softAssert is used.
     *
     * Steps:
     * 1. Navigate to bbc.com
     * 2. Click [Weather] button
     * 3. Enter 'New York' to the search input field
     * 4. Click search button
     * 5. {Verify} Search result contains link for 'New York, United States'
     * 6. Click on 'New York, United States' link
     * 7. {Verify} Current URL is "http://www.bbc.com/weather/5128581"
     * 8. {Verify} "NEW YORK" title is displayed
     * 9. {Verify} "Maps" block is displayed
     * 10. {Verify} "Average Conditions" block is displayed
     * 11. {Verify}"North America Forecast Video" block is displayed
     *
     */
    @Test(dataProvider = "searchTest")
    public void citySearchTest(String cityRequest, String cityResult, String cityURL) {
        DetailedCityPage detailedCityPage = mainPage.open()
                .clickOnWeatherLink()
                .search(cityRequest)
                .clickCityLink(cityResult);

        softAssert.assertTrue(driver.getCurrentUrl().equals(cityURL), "Incorrect page is displayed. URL should be " + cityURL + " but displayed " + driver.getCurrentUrl());
        softAssert.assertEquals(detailedCityPage.getLocationName(), cityResult, "Incorrect location name is displayed");
        softAssert.assertTrue(detailedCityPage.isMapsTabsBlockDisplayed(), "Maps block unexpectedly is not displayed");
        softAssert.assertTrue(detailedCityPage.isAverageConditionsBlockDisplayed(), "Average Conditions block unexpectedly is not displayed");
        softAssert.assertTrue(detailedCityPage.isForeCastVideosModuleDisplayed(), "Forecast Video block unexpectedly is not displayed");

        softAssert.assertAll();
    }

    @Test
    @Parameters({"cityRequestParam", "cityResultParam", "cityURLParam"})
    public void searchTestParameter(String cityRequest, String cityResult, String cityURL) {
        DetailedCityPage detailedCityPage = mainPage.open()
                .clickOnWeatherLink()
                .search(cityRequest)
                .clickCityLink(cityResult);

        softAssert.assertTrue(driver.getCurrentUrl().equals(cityURL), "Incorrect page is displayed. URL should be " + cityURL + " but displayed " + driver.getCurrentUrl());
        softAssert.assertEquals(detailedCityPage.getLocationName(), cityResult, "Incorrect location name is displayed");
        softAssert.assertTrue(detailedCityPage.isMapsTabsBlockDisplayed(), "Maps block unexpectedly is not displayed");
        softAssert.assertTrue(detailedCityPage.isAverageConditionsBlockDisplayed(), "Average Conditions block unexpectedly is not displayed");
        softAssert.assertTrue(detailedCityPage.isForeCastVideosModuleDisplayed(), "Forecast Video block unexpectedly is not displayed");

        softAssert.assertAll();
    }
}