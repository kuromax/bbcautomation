package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import pages.*;

import java.util.concurrent.TimeUnit;

/**
 * Created by mrybalkin on 09.01.2016.
 */
public class HeaderTests extends AbstractTest{
    protected MainPage mainPage;

    @BeforeMethod
    protected void beforeMethod() {
        super.beforeMethod();
        mainPage = new MainPage(driver);
    }

    /**
     * 1. Open BBC website
     * 2. Click News link
     * 3. {Verify} News page is displayed
     */
    @Test
    public void testOpenNewsPage(){
        NewsPage newsPage = mainPage.open()
                .clickOnNewsLink();

        softAssert.assertEquals(newsPage.getCurrentPageUrl(), newsPage.getNewsPageUrl(), "Unexpectedly News page is not displayed.");
        softAssert.assertTrue(newsPage.isWatchListenTextDisplayed(), "Unexpectedly text is not displayed.");

        softAssert.assertAll();
    }

    /**
     * 1. Open BBC website
     * 2. Click Shop link
     * 3. {Verify} Shop page is displayed
     */
    @Test
    public void testOpenShopPage(){
        ShopPage shopPage = mainPage.open()
                .clickOnShopLink();

        softAssert.assertEquals(shopPage.getCurrentPageUrl(), shopPage.getShopPageUrl(), "Unexpectedly Shop page is not displayed.");
        softAssert.assertTrue(shopPage.isFreeUkDeliveryTextIsDisplayed(), "Unexpectedly text is not displayed.");

        softAssert.assertAll();
    }

    /**
     * 1. Open BBC website
     * 2. Click Sport link
     * 3. {Verify} Sport page is displayed
     */
    @Test
    public void testOpenSportPage(){
        SportPage sportPage = mainPage.open()
                .clickOnSportLink();

        softAssert.assertEquals(sportPage.getCurrentPageUrl(), sportPage.getSportPageUrl(), "Unexpectedly Sport page is not displayed.");
        softAssert.assertTrue(sportPage.isVideoAudioTextDisplayed(), "Unexpectedly text is not displayed.");

        softAssert.assertAll();
    }

    /**
     * 1. Open BBC website
     * 2. Click Weather link
     * 3. {Verify} Weather page is displayed
     */
    @Test
    public void testOpenWeatherPage(){
        WeatherPage weatherPage = mainPage.open().clickOnWeatherLink();

        softAssert.assertEquals(weatherPage.getCurrentPageUrl(), weatherPage.getWeatherLinkUrl(), "Unexpectedly Weather page is not displayed.");
        softAssert.assertTrue(weatherPage.isWeatherVideoTextDisplayed(), "Unexpectedly text is not displayed.");

        softAssert.assertAll();
    }
}