package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;
import pages.MainPage;

import java.util.concurrent.TimeUnit;

/**
 * Created by mrybalkin on 20.12.2015.
 */
public class HeaderLinksTests {

    private static final String XPATH_NEWS_LINK = ".//*[@id='orb-nav-links']//*[text()='News']";
    private static final String XPATH_WATCH_LISTEN_TEXT = ".//*[@id='comp-pattern-library']//*[@class='group-title ']";
    private static final String XPATH_SPORT_LINK = ".//*[@id='orb-nav-links']//*[text()='Sport']";
    private static final String XPATH_VIDEO_AND_AUDIO_TEXT = "//*[@id='highlights-reports']//*[@class='carousel-header']";
    private static final String XPATH_WEATHER_LINK = ".//*[@id='orb-nav-links']//a[text()='Weather']";
    private static final String XPATH_WEATHER_VIDEO_TEXT = ".//*[@class='columns']//*[text()='Forecast Video']";
    private static final String XPATH_SHOP_LINK = ".//*[@id='orb-nav-links']//a[text()='Shop']";
    private static final String XPATH_FREE_UK_DELIVERY_TEXT = "//*[@id='fullWidthDelMessage']";

    private WebDriver driver;
    private SoftAssert softAssert;

    @BeforeTest
    private void beforeTest(){
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @BeforeMethod
    private void beforeMethod() {
        //open bbc home page

        driver.get("http://www.bbc.com/"); //TODO  лучше сделать через homePage.open();

        driver.get("http://www.bbc.com/");
        softAssert = new SoftAssert();

    }

    @Test
    public void testBbcNews(){
        String expectedURL = "http://www.bbc.com/news";

        //find 'News' button
        WebElement newsLink = driver.findElement(By.xpath(XPATH_NEWS_LINK)); //TODO Нехорошо xpath-ы в тестовом классе иметь, и это логика страницы.

        //clicking News button
        newsLink.click();

        // verifying expected url is opened
        Assert.assertEquals(driver.getCurrentUrl(), expectedURL, "URLs are different"); //TODO I believe it will be better to use SoftAssert here as well.

        // find 'Watch/Listen' text
        WebElement watchListen = driver.findElement(By.xpath(XPATH_WATCH_LISTEN_TEXT));  //TODO Нехорошо xpath-ы в тестовом классе иметь, и это логика страницы.

        // verifying expected text is displayed
        Assert.assertTrue(watchListen.isDisplayed(), "'Watch/Listen' is not displayed");//TODO I believe it will be better to use SoftAssert here as well.

        softAssert.assertAll();
    }

    @Test
    public void testBbcSport(){
        String expectedURL =  "http://www.bbc.com/sport/0/";

        //find 'Sports' button
        WebElement sportLink = driver.findElement(By.xpath(XPATH_SPORT_LINK));   //TODO Нехорошо xpath-ы в тестовом классе иметь, и это логика страницы.

        //clicking Sports button
        sportLink.click();

        // verifying expected url is opened
        Assert.assertEquals(driver.getCurrentUrl(), expectedURL, "URLs are different");//TODO I believe it will be better to use SoftAssert here as well.

        // find 'Video & Audio' text
        WebElement videoAndAudioText = driver.findElement(By.xpath(XPATH_VIDEO_AND_AUDIO_TEXT));   //TODO Нехорошо xpath-ы в тестовом классе иметь, и это логика страницы.

        // verifying expected text is displayed
        Assert.assertTrue(videoAndAudioText.isDisplayed(), "'Video & Audio' is not displayed");//TODO I believe it will be better to use SoftAssert here as well.

        softAssert.assertAll();
    }

    @Test
    public void testBbcWeather(){
        String expectedURL = "http://www.bbc.com/weather/";

        //find 'Weather' button
        WebElement weatherLink = driver.findElement(By.xpath(XPATH_WEATHER_LINK));  //TODO Нехорошо xpath-ы в тестовом классе иметь, и это логика страницы.

        //clicking Weather   button
        weatherLink.click();

        // verifying expected url is opened
        Assert.assertEquals(driver.getCurrentUrl(), expectedURL, "URLs are different");//TODO I believe it will be better to use SoftAssert here as well.

        // find 'Forecast Video' text
        WebElement weatherVideo = driver.findElement(By.xpath(XPATH_WEATHER_VIDEO_TEXT));  //TODO Нехорошо xpath-ы в тестовом классе иметь, и это логика страницы.

        // verifying expected text is displayed
        Assert.assertTrue(weatherVideo.isDisplayed(), "'Forecast Video' is not displayed");//TODO I believe it will be better to use SoftAssert here as well.

        softAssert.assertAll();
    }

    @Test
    public void testBbcShop(){
        String expectedURL = "http://www.bbcshop.com/";

        //find 'Shop' button
        WebElement shopLink = driver.findElement(By.xpath(XPATH_SHOP_LINK));  //TODO Нехорошо xpath-ы в тестовом классе иметь, и это логика страницы.

        //clicking Shop button
        shopLink.click();

        // verifying expected url is opened
        Assert.assertEquals(driver.getCurrentUrl(), expectedURL, "URLs are different");//TODO I believe it will be better to use SoftAssert here as well.

        // find 'FREE UK DELIVERY ON EVERYTHING' text
        WebElement freeUkDelivery = driver.findElement(By.xpath(XPATH_FREE_UK_DELIVERY_TEXT));  //TODO Нехорошо xpath-ы в тестовом классе иметь, и это логика страницы.

        // verifying expected text is displayed
        Assert.assertTrue(freeUkDelivery.isDisplayed(), "'FREE UK DELIVERY ON EVERYTHING' is not displayed");//TODO I believe it will be better to use SoftAssert here as well. 

        softAssert.assertAll();
    }

    @Test
    public void weatherLinkTest(){
        MainPage mainPage = new MainPage(driver);

        mainPage.open();
        mainPage.clickOnWeatherLink();

        Assert.assertEquals(driver.getCurrentUrl(), "http://www.bbc/com/weather");
    }

    @AfterMethod
    private void afterTest() {
        driver.quit();
    }
}