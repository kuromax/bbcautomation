package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.asserts.SoftAssert;

import java.util.concurrent.TimeUnit;

/**
 * Created by mrybalkin on 15.01.2016.
 */
public abstract class AbstractTest {

    protected static WebDriver driver;
    protected SoftAssert softAssert;

    @BeforeSuite
    public void beforeSuite(){
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @BeforeMethod
    protected void beforeMethod() {
        softAssert = new SoftAssert();
    }

    @AfterMethod
    protected void afterTest() {
        driver.quit();
    }
}
