package hw11;

/**
 * Created by mrybalkin on 09.01.2016.
 */
public class Eagle extends Animals implements Flyible {
    @Override
    public void voice() {
        System.out.println("I am the eagle and my name is " + name + ".");
    }

    public void fly() {
        System.out.println("I am the eagle and I fly away.");
    }
}
