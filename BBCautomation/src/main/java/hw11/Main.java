package hw11;

import java.util.ArrayList;

/**
 * Created by mrybalkin on 09.01.2016.
 */
public class Main {
    public static void main(String[] args) {
        ArrayList<Animals> animalList = new ArrayList<Animals>();

        for(int i = 0; i < 1; i++) {
            animalList.add(new Cat());
            animalList.add(new Dog());
            animalList.add(new Dolphin());
            animalList.add(new Eagle());
            animalList.add(new Owl());
            animalList.add(new Raccoon());
            animalList.add(new Whale());
        }

        for(int i = 0; i < animalList.size(); i++) {
            animalList.get(i).voice(); // One abstract class, one method - different implementation in different classes
        }

        ArrayList<Runnable> runnableList = new ArrayList<Runnable>();
        runnableList.add(new Cat());
        runnableList.add(new Dog());

        for(int i = 0; i < runnableList.size(); i++) {
            runnableList.get(i).run(); // One interface, one method - different implementation in different classes
        }
    }
}
