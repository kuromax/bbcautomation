package hw11;

/**
 * Created by mrybalkin on 09.01.2016.
 */
public class Owl extends Animals implements Flyible {
    @Override
    public void voice() {
        System.out.println("I am the owl and my name is " + name + ".");
    }

    public void fly() {
        System.out.println("I am the owl and currently is night so I fly to hunt");
    }
}
