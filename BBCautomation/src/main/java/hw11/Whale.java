package hw11;

/**
 * Created by mrybalkin on 09.01.2016.
 */
public class Whale extends Animals implements Swimmable {
    @Override
    public void voice() {
        System.out.println("I am the huge wheal and my name is " + name + ".");
    }

    public void swimm() {
        System.out.println("I am the wheal and I swim to eat plankton");
    }
}
