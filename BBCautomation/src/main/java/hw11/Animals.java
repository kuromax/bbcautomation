package hw11;

/**
 * Created by mrybalkin on 09.01.2016.
 */
public abstract class Animals {

    protected String name; // Visible in childs
    protected int age; // Visible in childs

    public void Animals(String name, int age){
        this.name = name;
        this.age = age;
    }

    public abstract void voice(); // Method without implementation (abstract)
}
