package hw11;

/**
 * Created by Acer on 09.01.2016.
 */
public class Dog extends Animals implements Runnable {
    @Override
    public void voice() {
        System.out.println("I am the dog and my name is " + name + ", gav - gav");
    }

    public void run() {
        System.out.println("I am the dog and I run, gav - gav");
    }

    public void fetch(){
        System.out.println("I am running for the stick, gav - gav");
    }
}
