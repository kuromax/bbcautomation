package hw11;

/**
 * Created by mrybalkin on 09.01.2016.
 */
public class Cat extends Animals implements Runnable{
    @Override
    public void voice() {
        System.out.println("I am the cat and my name is " + name + ", meow");
    }

    public void run() {
        System.out.println("I am the cat and I run as crazy, meow");
    }

    public void sleep(){
        System.out.println("I am the cat and I sleep ... Zzzzz ...");
    }
}
