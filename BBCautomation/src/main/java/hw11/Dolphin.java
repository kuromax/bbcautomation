package hw11;

/**
 * Created by mrybalkin on 09.01.2016.
 */
public class Dolphin extends Animals implements Swimmable {
    @Override
    public void voice() {
        System.out.println("I am the clever dolphin and my name is " + name + ".");
    }

    public void swimm() {
        System.out.println("I am the dolphin and I swim out to save people.");
    }
}
