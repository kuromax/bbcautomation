package hw11;

/**
 * Created by mrybalkin on 09.01.2016.
 */
public class Raccoon extends Animals implements Runnable {
    @Override
    public void voice() {
        System.out.println("I am the raccoon and my name is " + name + ", meow");
    }

    public void run() {
        System.out.println("I am the raccoon and I can run but I tumble");
    }

    public void steal(){
        System.out.println("I am the raccoon and I steal clothes");
    }
}
