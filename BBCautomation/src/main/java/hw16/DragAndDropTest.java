package hw16;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import tests.AbstractTest;

import java.util.List;

/**
 * Created by mrybalkin on 22.01.2016.
 */
public class DragAndDropTest extends AbstractTest {
    private static final String XPATH_SORTABLE_LI = "//ul[@id='sortable']/li";
    private Actions action;

    @BeforeMethod
    public void beforeTest(){
        super.beforeMethod();
        action = new Actions(driver);
        driver.get("file:///C:/Users/Acer/Desktop/drag_and_drop2/drag_and_drop2/drag_and_drop2/index.html");
    }

    /**
     * Test verifies:
     *  - block is not deleted after canceling action:
     *  - block is deleted after confirming action;
     *
     * Steps:
     * 1. Open test page
     * 2. Drag and drop any block to the "Trash"
     * 3. {Verify} Confirmation pop-up message is displayed and correct
     * 4. Click [Cancel] button to cancel delete action
     * 5. {Verify} Block is not deleted
     * 6. Drag and drop any block to the "Trash"
     * 7. Click [Ok} to confirm delete action
     * 8. {Verify} Block is deleted
     */
    @Test
    public void deleteOneElement(){

        List<WebElement> listOfElementsAtBegging = driver.findElements(By.xpath(XPATH_SORTABLE_LI));
        int listSize = listOfElementsAtBegging.size();

        WebElement first = driver.findElement(By.xpath("//ul[@id='sortable']/li[text()= '1']"));
        WebElement trash = driver.findElement(By.id("drop"));

        action.dragAndDrop(first, trash).perform();

        softAssert.assertEquals(driver.switchTo().alert().getText(), "Are you sure that you want to delete?");

        driver.switchTo().alert().dismiss();

        List<WebElement> ListOfElementsAfterDelete = driver.findElements(By.xpath(XPATH_SORTABLE_LI));
        int newListSize = ListOfElementsAfterDelete.size();

        softAssert.assertTrue(listSize == newListSize);

        action.dragAndDrop(first, trash).perform();
        driver.switchTo().alert().accept();

        ListOfElementsAfterDelete = driver.findElements(By.xpath(XPATH_SORTABLE_LI));
        newListSize = ListOfElementsAfterDelete.size();

        softAssert.assertTrue(listSize > newListSize);
        softAssert.assertAll();
    }

    /**
     * Test verifies it is possible to sort blocks
     *
     * 1. Sort blocks
     * 2. {Verify} sorting is correct
     */
    @Test
    public void sort(){
        List<WebElement> listOfElementsAtBegging = driver.findElements(By.xpath(XPATH_SORTABLE_LI));

        //sorting from 1 to 7. Moving element to li[element.text]
        for (int i = 0; i < listOfElementsAtBegging.size(); i++) {
            List<WebElement> listOfElementsAtBegging1 = driver.findElements(By.xpath(XPATH_SORTABLE_LI));

            int listInt = Integer.parseInt(listOfElementsAtBegging1.get(i).getText());
            action.dragAndDrop(listOfElementsAtBegging1.get(i), li(listInt)).perform();
        }

        for (int i =0; i < 7; i++){
            List<WebElement> listOfElementsAtBegging1 = driver.findElements(By.xpath(XPATH_SORTABLE_LI));
            Assert.assertEquals(listOfElementsAtBegging1.get(i).getText(), i + 1 + "");
        }

        //sorting from 7 to 1
        for (int i = listOfElementsAtBegging.size(); i > 0 ; i--) {
            List<WebElement> listOfElementsAtBegging1 = driver.findElements(By.xpath(XPATH_SORTABLE_LI));

            int listInt = Integer.parseInt(listOfElementsAtBegging1.get(i-1).getText());
            action.dragAndDrop(listOfElementsAtBegging1.get(i-1), li(8-listInt)).perform();
        }

        for (int i = 0; i < listOfElementsAtBegging.size(); i++){
            List<WebElement> listOfElementsAtBegging1 = driver.findElements(By.xpath(XPATH_SORTABLE_LI));
            Assert.assertEquals(listOfElementsAtBegging1.get(i).getText(), 7 - i + "");
        }
    }

    public WebElement li(int i){
        return driver.findElement(By.xpath("//ul[@id='sortable']/li["+ i +"]"));
    }
}